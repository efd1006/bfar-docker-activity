# bfar-docker-activity

BULLETIN BOARD MINI APP

//Auth 

@events
 - login
 - register

@fields
 - username
 - password

// Article / Post

@events
 - create

@fields
 - title
 - body
 - isPrivate

Any unauthenticated users must see public post, 
but if they they want to see private post they must login


SUBMIT HERE

172.16.0.202:9000